#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import db

class Code:
    TABLE_NAME = 'code'

    def __init__(self, code):
        self.__db = db.DB()
        self.__code = code
        self.operation_id = 0

    def find(self):
        query = "SELECT id, operation_id FROM %(table)s WHERE code = '%(code)s'" % {
            'table': self.TABLE_NAME,
            'code': self.__code
        }
        row = self.__db.getData(query)
        if len(row) > 0:
            self.operation_id = row[0][1]
            return True
        else:
            return False

    def insert(self):
        try:
            query = "INSERT INTO %(table)s (code, operation_id) VALUES ('%(code)s', '%(oper_id)s')" % {
                'table': self.TABLE_NAME,
                'code': self.__code,
                'oper_id': self.operation_id
            }
            self.__db.updateData(query)
            return True
        except:
            return False
