#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import mysql.connector
import config

class DB:

    def __connect(self):
        self.db = mysql.connector.connect(user=config.MYSQL_USER, password=config.MYSQL_PASS, host=config.MYSQL_HOST,
                                          port=config.MYSQL_PORT, database=config.MYSQL_DBNAME)
        self.cursor = self.db.cursor()

    def __disconnect(self):
        self.cursor.close()
        self.db.close()

    def getData(self, query):
        self.__connect()
        self.cursor.execute(query)
        row = self.cursor.fetchall()
        self.__disconnect()
        return row

    def updateData(self, query):
        self.__connect()
        self.cursor.execute(query)
        self.db.commit()
        self.__disconnect()