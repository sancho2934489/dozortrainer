#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import db

class Agent:
    TABLE_NAME = "agents"

    def __init__(self, uid):
        self.__uid = uid
        self.__db = db.DB()
        self.id = None
        self.username = None,
        self.first_name = None,
        self.last_name = None
        self.admin = 0

    def getAgent(self):
        query = "SELECT * FROM %(table)s WHERE uid = %(uid)d" % {
            'table': self.TABLE_NAME,
            'uid': self.__uid
        }
        row = self.__db.getData(query)
        print row
        if (len(row) == 0):
            return False
        else:
            agent = row[0]
            self.id = agent[0]
            self.username = agent[2]
            self.first_name = agent[3]
            self.last_name = agent[4]
            self.admin = agent[6]
            return True

    def createAgent(self):
        agent = self.getAgent()
        print agent
        if not agent:
            query = """INSERT INTO %(table)s (uid, create_date, username, first_name, last_name) 
                       VALUES ('%(uid)d', UNIX_TIMESTAMP(NOW()), '%(username)s', '%(first_name)s', '%(last_name)s')""" % {
                'table': self.TABLE_NAME,
                'uid': self.__uid,
                'username': self.username,
                'first_name': self.first_name,
                'last_name': self.last_name
            }
            self.__db.updateData(query)

    def updateAgent(self):
        agent = self.getAgent()
        if agent:
            query = "UPDATE %(table)s SET activity_date = UNIX_TIMESTAMP(NOW())" % {
                'table': self.TABLE_NAME
            }
            self.__db.updateData(query)