#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import db

class Logs:
    TABLE_NAME = 'logs'
    def __init__(self):
        self.db = db.DB()

    def insert(self, code, agent_id):
        query = "INSERT INTO %(table)s (code, time, agent_id) VALUES ('%(code)s', UNIX_TIMESTAMP(NOW()), '%(agent_id)s')" % {
            'table': self.TABLE_NAME,
            'code': code,
            'agent_id': agent_id
        }
        self.db.updateData(query)