#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import db,operation,time
from datetime import datetime

class agentOperation:
    TABLE_NAME = 'agent_operation'

    def __init__(self, agent_id):
        self.__db = db.DB()
        self.agent_id = agent_id
        self.operation_id = 0
        self.active = False
        self.start_date = None

        self.message = ''

    def start(self):
        operationObject = operation.Operation()
        operationObject.getActiveOperation()
        self.operation_id = operationObject.id

        query = "INSERT INTO %(table)s (agent_id, operation_id, active, start_date) VALUES ('%(agent_id)s', '%(operation_id)s', TRUE, UNIX_TIMESTAMP(NOW()))" % {
            'agent_id': self.agent_id,
            'table': self.TABLE_NAME,
            'operation_id': self.operation_id
        }
        try:
            self.__db.updateData(query)
            self.message = u'Операция %(name)s началась. У вас есть %(time)s минут на её выполнение.' % {
                'name': operationObject.name,
                'time': operationObject.timer / 60
            }
            return True
        except:
            return False

    def stop(self, operation_id):
        query = "UPDATE %(table)s SET active = FALSE WHERE operation_id = %(operation_id)s AND agent_id = %(agent_id)s" % {
            'table': self.TABLE_NAME,
            'operation_id': operation_id,
            'agent_id': self.agent_id
        }
        try:
            self.__db.updateData(query)
            self.message = u'Ваше время вышло'
            return True
        except:
            return False

    def get_time(self):
        operationObject = operation.Operation()
        operationObject.getActiveOperation()
        self.operation_id = operationObject.id
        timeNow = time.mktime(datetime.now().timetuple())

        query = "SELECT start_date FROM %(table)s WHERE agent_id = %(agent_id)s AND operation_id = %(operation_id)s AND active = TRUE" % {
            'table': self.TABLE_NAME,
            'agent_id': self.agent_id,
            'operation_id': self.operation_id
        }
        rows = self.__db.getData(query)
        if len(rows) > 0:
            start_date = rows[0][0]
            left_time = round((operationObject.timer - (timeNow - start_date)) / 60)
            if (left_time <= 0):
                self.stop(self.operation_id)
                return True
            else:
                self.message = u'До конца операции %(name)s осталось %(time)s минут' % {
                    'name': operationObject.name,
                    'time': left_time
                }
                return True
        else:
            return False