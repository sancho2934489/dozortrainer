#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import db

class Operation:
    TABLE_NAME = 'operation'

    def __init__(self):
        self.__db = db.DB()
        self.name = None
        self.active = 0
        self.id = 0
        self.timer = 0

    def create(self):
        try:
            query = "INSERT INTO %(table)s (name, create_data, active, timer) VALUES ('%(name)s', UNIX_TIMESTAMP(NOW()), '%(active)s', '%(timer)s')" % {
                'table': self.TABLE_NAME,
                'name': self.name,
                'active': self.active,
                'timer': self.timer
            }
            self.__db.updateData(query)
            return True
        except:
            return False

    def getOperationByName(self):
        query = "SELECT id FROM %(table)s WHERE name = '%(name)s'" % {
            'table': self.TABLE_NAME,
            'name': self.name
        }
        rows = self.__db.getData(query)
        if len(rows) == 0:
            return False
        else:
            self.id = rows[0][0]
            return True

    def getActiveOperation(self):
        query = "SELECT id, timer, name FROM %(table)s WHERE active = TRUE" % {
            'table': self.TABLE_NAME
        }
        rows = self.__db.getData(query)
        if len(rows) == 0:
            return False
        else:
            self.id = rows[0][0]
            self.timer = rows[0][1]
            self.name = rows[0][2]
            return True

    def getOperations(self):
        query = "SELECT name FROM %s WHERE active=TRUE" % self.TABLE_NAME
        rows = self.__db.getData(query)
        if len(rows) > 0:
            operations = ''
            for row in rows:
                operations += row[0] + '\n'
            return operations

        else:
            return 'Нет активных операций'

    def enable(self):
        try:
            query = "UPDATE %(table)s SET active = TRUE WHERE id = %(id)s" % {
                'table': self.TABLE_NAME,
                'id': self.id
            }
            self.__db.updateData(query)
            return True
        except:
            return False

    def disable(self):
        try:
            query = "UPDATE %(table)s SET active = FALSE WHERE id = %(id)s" % {
                'table': self.TABLE_NAME,
                'id': self.id
            }
            self.__db.updateData(query)
            return True
        except:
            return False