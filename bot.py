#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

from modules import config, agents, codes, logs, operation, agentOperation
import telebot, re

bot = telebot.TeleBot(config.BOT_KEY_API)

def agent(agent, message):
    agent.username = message.from_user.username
    agent.first_name = message.from_user.first_name
    agent.last_name = message.from_user.last_name
    agent.createAgent()



@bot.message_handler(commands=['get_operations'])
def get_operations(message):
    print message

    uid = message.from_user.id
    user = agents.Agent(uid)
    agent(user, message)
    user.getAgent()

    if user.admin:
        operationObject = operation.Operation()
        bot.send_message(message.chat.id, operationObject.getOperations())

@bot.message_handler(commands=['start_operation'])
def start_operation(message):
    print message
    uid = message.from_user.id
    user = agents.Agent(uid)
    agent(user, message)
    user.getAgent()

    agentOperationObject = agentOperation.agentOperation(user.id)
    if agentOperationObject.start():
        bot.send_message(message.chat.id, agentOperationObject.message)
    else:
        bot.send_message(message.chat.id, 'Произошла ошибка')

@bot.message_handler(content_types=["text"])
def get_code(message):
    print message
    log = logs.Logs()
    uid = message.from_user.id
    user = agents.Agent(uid)
    agent(user, message)

    text = message.text
    if re.match('^\!', text):
        code_text = re.sub('^!', '', text)
        code_text = re.sub('\*', 'D', code_text)
        code_text = re.sub('\#', 'R', code_text)
        code = codes.Code(code_text)
        log.insert(code_text, user.id)
        if code.find():
            bot.send_message(message.chat.id, 'Успех')
        else:
            bot.send_message(message.chat.id, 'Введен неверный код')

    if re.match(u'^время', text):
        agentOperationObject = agentOperation.agentOperation(user.id)
        if agentOperationObject.get_time():
            bot.send_message(message.chat.id, agentOperationObject.message)
        else:
            bot.send_message(message.chat.id, u'Произошла ошибка')


    if user.admin:
        if re.match(u'^создать операцию ', text):
            operationMas = re.split(',', re.sub(u'^создать операцию ', '', text))
            operationObject = operation.Operation()
            operationObject.name = operationMas[0]
            operationObject.active = operationMas[1]
            operationObject.timer = int(operationMas[2]) * 60
            if operationObject.create():
                bot.send_message(message.chat.id, u'Операция %s успешно создана' % operationObject.name)
            else:
                bot.send_message(message.chat.id, u'Произошла ошибка')
        if re.match(u'^добавить код ', text):
            codeMas = re.split(',', re.sub(u'^добавить код ', '', text))
            operationObject = operation.Operation()
            operationObject.name = codeMas[1]
            if (operationObject.getOperationByName()):
                codeObject = codes.Code(codeMas[0])
                codeObject.operation_id = operationObject.id
                if codeObject.insert():
                    bot.send_message(message.chat.id, u'Код %(code)s успешно добавлен в операцию %(oper)s' % {
                        'code': codeMas[0],
                        'oper': operationObject.name
                    })
                else:
                    bot.send_message(message.chat.id, u'Произошла ошибка')
            else:
                bot.send_message(message.chat.id, u'Нет такой операции')

        if re.match(u'^активировать операцию ', text):
            operationName = re.sub(u'^активировать операцию ', '', text)
            operationObject = operation.Operation()
            operationObject.name = operationName
            operationObject.getOperationByName()
            if operationObject.enable():
                bot.send_message(message.chat.id, u'Операция %s успешно активирована' % operationObject.name)
            else:
                bot.send_message(message.chat.id, u'Произошла ошибка')

        if re.match(u'^деактивировать операцию ', text):
            operationName = re.sub(u'^деактивировать операцию ', '', text)
            operationObject = operation.Operation()
            operationObject.name = operationName
            operationObject.getOperationByName()
            if operationObject.disable():
                bot.send_message(message.chat.id, u'Операция %s успешно деактивирована' % operationObject.name)
            else:
                bot.send_message(message.chat.id, u'Произошла ошибка')

if __name__ == '__main__':
    bot.polling(none_stop=True)